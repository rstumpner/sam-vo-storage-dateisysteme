---
marp: true
---

## Systemadministration
### Vorlesung

---
## Storage 
### Dateisysteme


---
### Übersicht Dateisysteme
* Organisation von Daten (Dateisysteme)
* Dateisystem Generationen
* Verteilte Dateisysteme

---
### Organisation von Daten
(Dateisysteme)

#### Um die Daten wiederzufinden wird eine Art Datenbank benötigt. Moderne Dateisysteme bieten eine Fülle an Features um die Organisation von Daten zu unterstützen.

---
### Dateisysteme Generation-0
* Am Anfang war das Chaos
* Kein System
* Organisation die Ablage

---
## Beispiele
* ATARI ROM Cards
* Lochkarten

---
## Lochkarten
![Lochkaren Bild](_images/dateisysteme-g0-1.png)

---
## ATARI Speicherkarten
![Atari Speicherkarte](_images/dateisysteme-g0-2.png)

---
### Dateisysteme Generation-1
* Ablage in Dateien
* Keine Metadaten
* Keine Ordner
* Killerfeature: Dateiname

---
## Beispiele
* C64
* CMP

---
### Dateisysteme Generation-2
 
* erste Möglichkeiten der Organisation
* Trennung von Daten (Blöcke) und Metadaten (Dateiname)
* Killerfeature der Ordner

---
## Beispiele
* FAT-16
* FAT-32

---
## FAT-16
* Max. 2 GB Datei
* Max. 4 GB Partition

---
## FAT-32
* Max. 4 GB Datei
* Max. 8 TB Partition

---
### FAT-32 Aufbau
* Botsektor (HEX00)
* File Allocation Table 1
* File Allocation Table 2
* Root Verzeichnis (FAT-16)
* Datenbereich

---
## Partitionierung
* Master Boot Record (MBR)
* GUID Partition Table (GPT)

---
### Master Boot Record
* Botsektor HEX00 (512 Byte)
* bis 2 TB
* Primär Partitionen (4)
* Logische Partitionen (4)
  
---
### Master Boot Record

![MBR Skizze](_images/partition-mbr.png)

---
### GUID Partition Table
* Global Unique Identifier Parition Table
* Botsektor HEX00 (512 Byte)
* Protected MBR (Kompatibiltät)
* Primary GPT Header
* Secondary GPT Header (Kopie)
* 128+ Partitionen
* 8 Zebibyte
* CRC32 Prüfsumme
  
---
### GUID Partition Table

![GPT Skizze](_images/partition-gpt.png)

---
### Dateisysteme Generation-3
* Permissions
* Belibige Metadaten
* Streams

---
## Beispiele
* NTFSv1
* EXT2

---
## NTFSv1
* Max. 16 TB Datei
* Max. 256 TB Partition

---
### Dateisysteme Generation-4
* Journaling (Transaktion)

---
## Beispiele:
* NTFSv3
* EXT4

---
## NTFSv3
* Max. 16 TB Datei
* Max. 256 TB Partition

---
### Dateisysteme Generation-5
* Copy on Write
* Pools (Build in LVM)
* Snapshots
* RAID / Erasured Encoding (RAID-Z)
* Tiering (SLOG / Cache)
* Kompression
* Deduplizierung

---
## Beispiele:
* ZFS
* BTRFS
* REFS

---
### Verteilte Dateisysteme
* Shared Disk Dateisysteme / Cluster Dateisysteme
* Sync and Share
* Paralell Cluster Dateisysteme

---
### Shared Disk Dateisysteme
#### Viele Hosts greifen auf das gleiche Blockdevice zu Ermöglicht einem Rechencluster konkurierenden Zugriff auf ein gemeinsames Block Device.

---
## Eigenschaften
* Distributed Locking Mechanismus ist notwendig
* Auch Cluster Dateisysteme genannt
* Vorteile in der Geschwindigkeit gegenüber CIFS / NFS
* Verwendung bei High Avalability Anforderungen
* Verwendung im Webserver Bereich

---
### Beispiele
* GFS2
* OCFS2
* CXFS
* VMFS

---
### Shared Disk Dateisysteme
![Shared Disk Dateisysteme](_images/sam-verteilte-dateisysteme.jpg)

---
### Objektbasierte Dateisysteme
* Alles ist ein Objekt
* Eine Datei wird dann in zum Beispiel 4 K große Objekte geteilt
* Get / Put / Delete
* Keine Verzeichnishirachie
* Namensräume

---
### Beispiele
* Amazon S3
* CEPH
* GLUSTERFS

---
#### Beispiel CEPH

![CEPH Skizze](_images/sam-objekt-store-ceph.png)

---
## Sync and Share
### Dateien werden zwischen den Hosts Syncronisiert

* Versionierung
* Repository basierend
* Meist ein Agent Notwendig

---
### Beispiele
* Rsync
* Syncthing
* Seafile
* Nextcloud
* Dropbox

---
### Paralell Cluster Dateisysteme
* Verwendung im HPC Bereich
* Massive Paralellzugriffe möglich
* Scalierbarkeit

---
### Beispiele
* Beegfs
* Lizzardfs
* IBM GPFS
* Lustre

---
# Fragen ???

---
#### Links
###### Dateisysteme Allgemein
* https://de.wikipedia.org/wiki/File_Allocation_Table
* https://de.wikipedia.org/wiki/Master_Boot_Record
* https://de.wikipedia.org/wiki/GUID_Partition_Table
* https://en.wikipedia.org/wiki/Erasure_code
* https://en.wikipedia.org/wiki/Comparison_of_file_systems

---
#### Links
###### Dateisysteme
* https://de.wikipedia.org/wiki/Ext4
* https://en.wikipedia.org/wiki/ZFS
* https://en.wikipedia.org/wiki/Btrfs
* https://de.wikipedia.org/wiki/ReFS
* https://de.wikipedia.org/wiki/NTFS-3G

---
#### Links
###### Dateisysteme (Cluster)
* https://de.wikipedia.org/wiki/Global_File_System
* https://de.wikipedia.org/wiki/OCFS2
* https://de.wikipedia.org/wiki/CXFS
* https://de.wikipedia.org/wiki/Ceph
* https://de.wikipedia.org/wiki/GlusterFS

---
#### Links
###### Dateisysteme (sync)

* https://de.wikipedia.org/wiki/Rsync
* https://syncthing.net
* https://www.seafile.com/en/home/
* https://nextcloud.com/
* https://www.dropbox.com
  
---
#### Links
###### Dateisysteme (paralell)

* https://lizardfs.com
* https://de.wikipedia.org/wiki/BeeGFS
* https://de.wikipedia.org/wiki/IBM_General_Parallel_File_System
* https://de.wikipedia.org/wiki/Lustre_(Dateisystem)

---
## Fragen

- Skizzieren sie die Komponenten eines Shared Disk Cluster Filesystems und nenne 2 Verwendungsmöglichkeiten
- Nennen sie mindestens 2 Generationen von Filesystemen und ihre Merkmale
- Nennen sie mindestens 4 Eigenschaften eines Dateisystems der 5. Generation
- Nennen sie 4 Eigenschaften eines Shared Disk Dateisystems